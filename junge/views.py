from django.shortcuts import render_to_response,render
from junge import models
from django.http import HttpResponseRedirect,HttpResponse
from django.contrib import auth
from django_comments import models as comments_models

def index(request):
    # 从数据库中取出bbs数据渲染到index.html中
    bbs_list=models.BBS.objects.order_by('created_at')
    #bbs_list=list()
    n=0
    # for i in l:
    #     bbs_list.append(i)
    #     n+=1
    #     if n >=20:
    #         break

    # 取出bbs的类别
    bbs_categories = models.Category.objects.all()
    bbs_user=models.BBS_user.objects.all()
    bbs_id=0
    user=request.user
    return  render(request,'index.html',locals())


# bbs分类
def category(request, cate_id):
    bbs_list = models.BBS.objects.filter(category__id=cate_id)
    bbs_categories = models.Category.objects.all()
    return render_to_response('index.html', {'bbs_list': bbs_list,
                                             'user': request.user,
                                             'bbs_category': bbs_categories,
                                             'bbs_id': int(cate_id),
                                             })


def bbs_detail(request,d):
    bbs_obj=models.BBS.objects.get(id=d)
    bbs_obj.view_count+=1
    bbs_obj.save()
    user=request.user
    return render(request,'bbs_detail.html',locals())

def sub_comment(request):
    bbs_id=request.POST.get('bbs_id')
    comment=request.POST.get('comment_content')
    c=comments_models.Comment()
    c.comment=comment
    c.object_pk=bbs_id
    c.content_type_id=9
    c.site_id=1
    c.user=request.user
    comments_models.Comment.save(c)
    return HttpResponseRedirect('/detail/%s' %bbs_id)

def acc_login(request):
    username=request.POST.get('username')
    password=request.POST.get('password')
    user=auth.authenticate(username=username,password=password)
    if user is not None:
        auth.login(request,user)
        content='''
        Welcome %s !!!
        <a href='/logout/' >Logout</a>
        '''%user.username
        return HttpResponseRedirect('/')

def logout_view(request):
    user=request.user
    auth.logout(request)
    return HttpResponse("<b>%s</b> logge out! <br/><a href='/'>Re_login</a>" %user)
def Login(request):
    return render(request, 'zh.html')

def bbs_pub(request):

    return render(request,'sub_pub.html')

def bbs_sub(request):
    author=models.BBS_user.objects.get(user__username=request.user)
    picture=request.POST.get('summary_pictrue')
    title = request.POST.get('title')
    content = request.POST.get('content')
    summary = request.POST.get('summary')
    models.BBS.objects.create(
        title=title,
    summary = summary,
    content = content,
    author = author,
    picture=picture
    )
    return HttpResponse("<h2>Bbs was published, please enter <br/><a href='/'>return</a> to index!<h2>")

def reg(request):
    return render(request,'zhreg.html')


def log(request):
    return render(request,'zh.html')

def acc_reg(request):
    name=request.POST.get('username')
    pawd=request.POST.get('password')
    u=models.User(username=name)
    u.set_password(pawd)
    u.save()
    obj=models.BBS_user()
    obj.user=u
    obj.save()
    # obj=models.BBS_user.user
    #
    # obj.Username=name
    # obj.Password=pawd
    # obj.save()
    # obj=models.BBS_user.objects.get(user__username=request.user)
    # models.BBS_user.objects.create(
    #     Username=request.GET('username'),
    #     Password=request.GET('password'),
    # )
    # obj.username=request.GET('username')
    # obj.password=request.GET('password')
    # obj.save()
    return HttpResponse('注册成功！')

def technology(request):
    return render(request,'technology.html')

def hot(request):
    return render(request,'hot.html')

def home(request):
    user=models.BBS_user.objects.get(user__username=request.user)
    print(user.photo)
    return render(request,'home.html',locals())