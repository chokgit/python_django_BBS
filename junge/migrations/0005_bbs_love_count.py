# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-21 10:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('junge', '0004_bbs_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='bbs',
            name='love_count',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
