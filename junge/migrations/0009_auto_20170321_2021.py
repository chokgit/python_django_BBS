# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-21 12:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('junge', '0008_auto_20170321_2020'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bbs',
            name='ranking',
            field=models.IntegerField(default=0),
        ),
    ]
